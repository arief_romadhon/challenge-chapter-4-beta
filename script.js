let hero = ['batu', 'gunting', 'kertas'];
let batu = document.getElementById('batu');
let gunting = document.getElementById('gunting');
let kertas = document.getElementById('kertas');
let info = document.getElementById('status');
let classActive = document.getElementsByClassName('active');
let restartButton = document.getElementById('restart');


let start = 'VS';

batu.addEventListener('click', function(e){
    if (classActive.length < 2) {
        batu.classList.add('active');
        gunting.classList.remove('active');
        kertas.classList.remove('active');
        suit ('batu');
    }
});

gunting.addEventListener('click', function(e){
    if (classActive.length < 2){
        gunting.classList.add('active');
        batu.classList.remove('active');
        kertas.classList.remove('active');
        suit('gunting');
    }
});

kertas.addEventListener('click', function(e){
    if (classActive.length < 2){
        kertas.classList.add('active');
        gunting.classList.remove('active');
        batu.classList.remove('active');
        suit('kertas')
    }
})

restartButton.addEventListener('click', function(){
    gameRestart();
})

function gameRestart () {
    info.textContent = start;
    batu.classList.remove('active');
    gunting.classList.remove('active');
    kertas.classList.remove('active');

    hero.forEach((item) => {
        let comClear = document.getElementById(`${item}-com`);
        comClear.classList.remove('active');
    })
}

function suit (pilihan){
    if (classActive.length < 2) {
        let player = pilihan;

        let random = Math.floor(Math.random() * 3);
        console.log(random);
        let com = hero[random];
    
        hero.forEach((item) => {
            let comClear = document.getElementById(`${item}-com`);
            comClear.classList.remove('active');
        })
    
        let comActive = document.getElementById(`${com}-com`);
        comActive.classList.add('active')
    
        let result = winner (player, com);
        info.textContent = result;
        if ( info.textContent = result) {
            info.style.textAlign = 'center';
            info.style.width = '260px';
            info.style.height = '150px';
            info.style.fontSize='38px';
            info.style.backgroundColor = 'green';
            info.style.color = 'white';
            info.style.padding = '40px';
            info.style.borderRadius = '10px';
            info.style.transform = 'rotate(-28.87deg)';
        } 
    }
    // alert(`player memilih ${player} vs computer memilih ${com}, maka hasilnya : ${result}`)
}


function winner (player, com){
    if(player == com){
        return 'DRAW'
    } if(player == 'batu'){
        return (com == 'gunting') ? 'PLAYER 1 WIN' : 'COM WIN';
    } if(player == 'gunting'){
        return (com == 'kertas') ? 'PLAYER 1 WIN' : 'COM WIN';
    } if(player == 'kertas'){
        return (com == 'batu') ? 'PLAYER 1 WIN' : 'COM WIN';
    }
}
